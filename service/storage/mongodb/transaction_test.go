package mongodb

import (
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
	"github.com/stretchr/testify/assert"
)

func TestWeCanParseDomainTransactionsToDb(t *testing.T) {
	baseAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ac")
	otherAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ad")

	txs := []*domain.Transaction{
		{
			From:   baseAddress,
			To:     otherAddress,
			Amount: common.Big1,
		},
		{
			From:   otherAddress,
			To:     baseAddress,
			Amount: common.Big2,
		},
	}

	txsDb := ParseTransactionsToDb(txs)

	expectedTxs := []*dbTransaction{
		{
			From:   baseAddress,
			To:     otherAddress,
			Amount: "1",
		},
		{
			From:   otherAddress,
			To:     baseAddress,
			Amount: "2",
		},
	}

	assert.Equal(t, expectedTxs, txsDb)
}

func TestWeCanConvertDbTxsToNormalTxs(t *testing.T) {
	baseAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ac")
	otherAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ad")

	dbTxs := []*dbTransaction{
		{
			From:   baseAddress,
			To:     otherAddress,
			Amount: "1",
		},
		{
			From:   otherAddress,
			To:     baseAddress,
			Amount: "2",
		},
	}

	txs := ParseDbToTransactions(dbTxs)

	expectedTxs := []*domain.Transaction{
		{
			From:   baseAddress,
			To:     otherAddress,
			Amount: common.Big1,
		},
		{
			From:   otherAddress,
			To:     baseAddress,
			Amount: common.Big2,
		},
	}

	assert.Equal(t, expectedTxs, txs)
}
