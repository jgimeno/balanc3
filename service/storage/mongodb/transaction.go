package mongodb

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
)

type dbTransaction struct {
	From   common.Address
	To     common.Address
	Amount string
}

type dbTransactionRegister struct {
	Address common.Address
	From    []*dbTransaction
	To      []*dbTransaction
}

type dbBalance struct {
	Address common.Address
	Balance string
}

//ParseDbToTransactions it is needed to do a conversion from big int to string in order to correctly save
//a big int into mongo. Needs little bit more of research.
func ParseTransactionsToDb(txs []*domain.Transaction) []*dbTransaction {
	var dbTxs []*dbTransaction

	for _, tx := range txs {
		dbTxs = append(dbTxs, &dbTransaction{
			From:   tx.From,
			To:     tx.To,
			Amount: tx.Amount.String(),
		})
	}

	return dbTxs
}

func ParseDbToTransactions(txs []*dbTransaction) []*domain.Transaction {
	var realTxs []*domain.Transaction

	for _, tx := range txs {
		amount := new(big.Int)
		//@TODO: ugly fast solution to converting big int from string, need to check the error.
		amount, _ = amount.SetString(tx.Amount, 10)

		realTxs = append(realTxs, &domain.Transaction{
			From:   tx.From,
			To:     tx.To,
			Amount: amount,
		})
	}

	return realTxs
}
