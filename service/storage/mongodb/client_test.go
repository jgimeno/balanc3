package mongodb

import (
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
	"github.com/jgimeno/gomondoctest"
	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestItSavesTheTransactions(t *testing.T) {
	gm := gomondoctest.NewGomondoc(t)

	gm.RunMongo()
	defer gm.StopMongo()

	repo := repository{}

	t.Run("Saving one transaction register", func(t *testing.T) {
		register := &domain.TransactionRegister{
			Address: common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca754"),
			From: []*domain.Transaction{
				{
					From:   common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca754"),
					To:     common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca753"),
					Amount: big.NewInt(1),
				},
			},
			To: []*domain.Transaction{},
		}

		err := repo.SaveTransactionRegister(register)
		assert.NoError(t, err)

		savedRegister, err := repo.GetTransactionRegister(register.Address)
		assert.NoError(t, err)

		checkRegistersAreEqual(t, register, savedRegister)
	})

	t.Run("Saving another registry does not duplicate", func(t *testing.T) {
		register := &domain.TransactionRegister{
			Address: common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca754"),
			From:    []*domain.Transaction{},
			To: []*domain.Transaction{
				{
					From:   common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca754"),
					To:     common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca753"),
					Amount: big.NewInt(1),
				},
			},
		}

		err := repo.SaveTransactionRegister(register)
		assert.NoError(t, err)

		session, err := mgo.Dial("")
		query := session.DB(DbName).C(TxsCollection).Find(bson.M{"address": register.Address})

		n, err := query.Count()
		assert.NoError(t, err)

		assert.Equal(t, 1, n)
	})
}

func checkRegistersAreEqual(t *testing.T, expected *domain.TransactionRegister, reg *domain.TransactionRegister) {
	assert.Equal(t, expected.Address, reg.Address)
	for i, f := range reg.From {
		assert.Equal(t, expected.From[i].From, f.From)
		assert.Equal(t, expected.From[i].To, f.To)
		assert.Equal(t, expected.From[i].Amount, f.Amount)
	}

	for i, to := range reg.To {
		assert.Equal(t, expected.To[i].From, to.From)
		assert.Equal(t, expected.To[i].To, to.To)
		assert.Equal(t, expected.To[i].Amount, to.Amount)
	}
}

func TestItSavesAndGetsBalance(t *testing.T) {
	gm := gomondoctest.NewGomondoc(t)

	gm.RunMongo()
	defer gm.StopMongo()

	account := common.HexToAddress("0xbf4ed7b27f1d666546e30d74d50d173d20bca754")
	balance := big.NewInt(1)

	repo := New(":27017")

	t.Run("we can save a new balance", func(t *testing.T) {
		err := repo.SaveBalance(account, balance)
		assert.NoError(t, err)
	})

	t.Run("we can get the balance", func(t *testing.T) {
		savedBalance, err := repo.GetBalance(account)
		assert.NoError(t, err)

		assert.Equal(t, balance, savedBalance)
	})
}
