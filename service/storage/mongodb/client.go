package mongodb

import (
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
	"github.com/jgimeno/balanc3/service/storage"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	DbName             = "test"
	TxsCollection      = "transactions"
	BalancesCollection = "balances"
)

type repository struct {
	server string
}

func (r *repository) GetBalance(address common.Address) (*big.Int, error) {
	session, err := mgo.Dial(r.server)
	if err != nil {
		return nil, fmt.Errorf("error connecting to mongo: %s", err)
	}
	defer session.Close()

	dbBalance := dbBalance{}
	c := session.DB(DbName).C(BalancesCollection)

	err = c.Find(bson.M{"address": address}).One(&dbBalance)
	if err != nil {
		return nil, fmt.Errorf("error getting the var %s", err)
	}

	bIntBalance := new(big.Int)
	bIntBalance, ok := bIntBalance.SetString(dbBalance.Balance, 10)
	if !ok {
		return nil, fmt.Errorf("problem getting balance: %s", err)
	}

	return bIntBalance, nil
}

func (r *repository) SaveBalance(address common.Address, balance *big.Int) error {
	session, err := mgo.Dial(r.server)
	if err != nil {
		return fmt.Errorf("error connecting to mongo: %s", err)
	}
	defer session.Close()

	c := session.DB(DbName).C(BalancesCollection)
	dbBalance := dbBalance{
		Address: address,
		Balance: balance.String(),
	}

	_, err = c.Upsert(bson.M{"address": address}, dbBalance)
	if err != nil {
		return fmt.Errorf("error saving account balance to mongo: %s", err)
	}

	return nil
}

func (r *repository) SaveTransactionRegister(register *domain.TransactionRegister) error {
	session, err := mgo.Dial(r.server)
	if err != nil {
		return fmt.Errorf("error connecting to mongo: %s", err)
	}
	defer session.Close()

	c := session.DB(DbName).C(TxsCollection)

	dbRegister := dbTransactionRegister{
		Address: register.Address,
		From:    ParseTransactionsToDb(register.From),
		To:      ParseTransactionsToDb(register.To),
	}

	_, err = c.Upsert(bson.M{"address": register.Address}, dbRegister)
	if err != nil {
		return fmt.Errorf("error saving transaction register to mongo: %s", err)
	}

	return nil
}

func (r *repository) GetTransactionRegister(address common.Address) (*domain.TransactionRegister, error) {
	session, err := mgo.Dial(r.server)
	if err != nil {
		return nil, fmt.Errorf("error connecting to mongo: %s", err)
	}
	defer session.Close()

	dbRegister := dbTransactionRegister{}
	c := session.DB(DbName).C(TxsCollection)

	err = c.Find(bson.M{"address": address}).One(&dbRegister)
	if err != nil {
		return nil, fmt.Errorf("error getting the var %s", err)
	}

	return &domain.TransactionRegister{
		Address: dbRegister.Address,
		From:    ParseDbToTransactions(dbRegister.From),
		To:      ParseDbToTransactions(dbRegister.To),
	}, nil
}

func New(server string) storage.TransactionRepository {
	return &repository{
		server: server,
	}
}
