package storage

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
)

type TransactionRepository interface {
	SaveTransactionRegister(register *domain.TransactionRegister) error
	GetTransactionRegister(address common.Address) (*domain.TransactionRegister, error)
	SaveBalance(address common.Address, balance *big.Int) error
	GetBalance(address common.Address) (*big.Int, error)
}
