package explorer

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
)

type ChainExplorer interface {
	GetTransactions(address common.Address) (*domain.TransactionRegister, error)
	GetBalance(address common.Address) (*big.Int, error)
}
