package etherscan

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
	"github.com/jgimeno/balanc3/service/explorer"
	"github.com/nanmu42/etherscan-api"
	"github.com/pkg/errors"
)

type client struct {
	config    *Config
	ethClient *etherscan.Client
}

type Config struct {
	ApiKeyToken string
}

func New(config *Config) (explorer.ChainExplorer, error) {
	if config.ApiKeyToken == "" {
		return nil, errors.New("apikey cannot be empty")
	}

	return &client{
		config:    config,
		ethClient: etherscan.New(etherscan.Mainnet, config.ApiKeyToken),
	}, nil
}

func (c *client) GetTransactions(address common.Address) (*domain.TransactionRegister, error) {
	txs, err := c.ethClient.NormalTxByAddress(address.String(), nil, nil, 0, 0, false)
	if err != nil {
		return nil, err
	}

	var fromTxs []*domain.Transaction
	var toTxs []*domain.Transaction

	for _, tx := range txs {
		tx := &domain.Transaction{
			From:   common.HexToAddress(tx.From),
			To:     common.HexToAddress(tx.To),
			Amount: (*big.Int)(tx.Value),
		}

		if tx.From == address {
			fromTxs = append(fromTxs, tx)
		} else if tx.To == address {
			toTxs = append(toTxs, tx)
		}
	}

	return &domain.TransactionRegister{
		Address: address,
		From:    fromTxs,
		To:      toTxs,
	}, nil
}

func (c *client) GetBalance(address common.Address) (*big.Int, error) {
	balance, err := c.ethClient.AccountBalance(address.String())
	if err != nil {
		return nil, err
	}

	return (*big.Int)(balance), nil
}
