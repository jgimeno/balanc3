package etherscan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestItFailsIfCallingTheClientWithoutToken(t *testing.T) {
	_, err := New(&Config{})
	assert.Error(t, err)
}
