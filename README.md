### Executing the project

To run the project we need to have an instance of mongo running somewhere.
The easiest is to have docker installed on the machine and run from the root of the project:

```
make run
```

After it we will have a container running mongo on our machine in default port 27017.

Now we can run the application with:

```
go run cmd/main.go -apikey API_KEY -mongo :27017
```

API_KEY is a etherscan api key, and :27017 is the address of the mongo server, in this case is in our machine so :27017 is enough.


### Running tests

To run tests, because it includes some integratios too is necessary to have docker on the machine
since it uses an instance of mongo.

```
go test ./...
```

### Endpoints

The application exposes 3 uses cases (endpoints)

#### Syncing an address

Syncing an address is like getting all data related to an address and save it to
internal mongodb so we can fetch it later.

```
Example:

http://localhost:8080/sync/0xa10a3f19b7fc4a634eab504a3625d3ebc8fae3f2
```

It returns status 200 if everything was good.

#### Getting balance of address

We can get the balance of an address after syncing with:

```
Example:

http://localhost:8080/balance/0xa10a3f19b7fc4a634eab504a3625d3ebc8fae3f2
```


#### Getting txs list for an addreess

We can get the transactions from an address, divided by the transactions to that address and from that address.


```
Example:

http://localhost:8080/txs/0xa10a3f19b7fc4a634eab504a3625d3ebc8fae3f2
```


And that is all, lots of improvements can be done in the app, but the description said
to use around 5 to 6 hours so I did not want to go too much in the detail.

There are some tests, another ones that needed mock were avoided to not lose too much time
they were tested live and after it removed so it does not depend on an apikey for
etherscan.
