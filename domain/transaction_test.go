package domain

import (
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

func TestWeCanGetTransactionsFromAccount(t *testing.T) {
	baseAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ac")
	otherAddress := common.HexToAddress("0x123f681646d4a755815f9cb19e1acc8565a0c2ad")

	txs := TransactionRegister{
		Address: baseAddress,
		From: []*Transaction{
			{
				From:   baseAddress,
				To:     otherAddress,
				Amount: common.Big1,
			},
		},
		To: []*Transaction{
			{
				From:   otherAddress,
				To:     baseAddress,
				Amount: common.Big1,
			},
		},
	}

	expectedFrom := []*Transaction{
		{
			From:   baseAddress,
			To:     otherAddress,
			Amount: common.Big1,
		},
	}

	expectedTo := []*Transaction{
		{
			From:   otherAddress,
			To:     baseAddress,
			Amount: common.Big1,
		},
	}

	assert.Equal(t, expectedFrom, txs.From)
	assert.Equal(t, expectedTo, txs.To)
}
