package domain

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
)

type Transaction struct {
	From   common.Address `json:"from"`
	To     common.Address `json:"to"`
	Amount *big.Int       `json:"amount"`
}

type TransactionRegister struct {
	Address common.Address `json:"address"`
	From    []*Transaction `json:"from"`
	To      []*Transaction `json:"to"`
}
