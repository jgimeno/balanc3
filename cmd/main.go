package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/jgimeno/balanc3/app/api"
	"github.com/jgimeno/balanc3/service/explorer/etherscan"
	"github.com/jgimeno/balanc3/service/storage/mongodb"
)

func main() {
	var apiKey = flag.String("apikey", "", "The etherescan apikey.")
	var mongoDbAddr = flag.String("mongo", "", "The mongodb server address.")

	flag.Parse()

	if *apiKey == "" {
		fmt.Printf("Etherescan apikey is mandatory.\n")
		os.Exit(1)
	}

	if *mongoDbAddr == "" {
		fmt.Printf("Address for mongo db server is mandatory.\n")
		os.Exit(1)
	}

	client, err := etherscan.New(&etherscan.Config{ApiKeyToken: *apiKey})
	if err != nil {
		os.Exit(1)
	}
	repository := mongodb.New(*mongoDbAddr)

	router := api.CreateRouter(client, repository)

	err = http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Printf("error executing microservice: %s", err)
	}
}
