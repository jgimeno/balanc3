package api

type GetBalanceResponse struct {
	Balance string `json:"balance"`
}

type SuccessResponse struct {
	Message string `json:"message"`
}
