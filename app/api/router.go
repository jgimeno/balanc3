package api

import (
	json2 "encoding/json"
	"fmt"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"github.com/gorilla/mux"
	"github.com/jgimeno/balanc3/app/command"
	"github.com/jgimeno/balanc3/service/explorer"
	"github.com/jgimeno/balanc3/service/storage"
)

func CreateRouter(chainExplorer explorer.ChainExplorer, repository storage.TransactionRepository) http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/sync/{address}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)

		address := vars["address"]
		if address == "" {
			http.Error(writer, fmt.Sprintf("address is mandatory"), http.StatusBadRequest)
			return
		}

		handler := command.NewSyncAddressHandler(chainExplorer, repository)
		err := handler.Handle(common.HexToAddress(address))
		if err != nil {
			http.Error(writer, fmt.Sprintf("error syncing address: %s", err), http.StatusBadRequest)
			return
		}

		response := SuccessResponse{
			Message: fmt.Sprintf("address %s correctly synchronized", address),
		}

		json, err := json2.Marshal(response)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error creating response: %s", err), http.StatusBadRequest)
			return
		}

		_, err = writer.Write(json)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error writing response: %s", err), http.StatusBadRequest)
			return
		}
	})

	router.HandleFunc("/balance/{address}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)

		address := vars["address"]
		if address == "" {
			http.Error(writer, fmt.Sprintf("address is mandatory"), http.StatusBadRequest)
			return
		}

		handler := command.NewGetBalanceHandler(repository)
		balance, err := handler.Handle(common.HexToAddress(address))
		if err != nil {
			http.Error(writer, fmt.Sprintf("error getting balance: %s", err), http.StatusBadRequest)
			return
		}

		response := GetBalanceResponse{
			Balance: balance.String(),
		}

		json, err := json2.Marshal(&response)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error creating response: %s", err), http.StatusBadRequest)
			return
		}

		_, err = writer.Write(json)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error writing response: %s", err), http.StatusBadRequest)
			return
		}
	})

	router.HandleFunc("/txs/{address}", func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)

		address := vars["address"]
		if address == "" {
			http.Error(writer, fmt.Sprintf("address is mandatory"), http.StatusBadRequest)
			return
		}

		handler := command.NewGetTransactionsHandler(repository)
		txs, err := handler.Handle(common.HexToAddress(address))
		if err != nil {
			http.Error(writer, fmt.Sprintf("error creating response: %s", err), http.StatusBadRequest)
			return
		}

		json, err := json2.Marshal(&txs)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error creating response: %s", err), http.StatusBadRequest)
		}

		_, err = writer.Write(json)
		if err != nil {
			http.Error(writer, fmt.Sprintf("error writing response: %s", err), http.StatusBadRequest)
			return
		}
	})

	return router
}
