package command

import (
	"fmt"
	"math/big"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/service/storage"
)

type GetBalanceHandler struct {
	repository storage.TransactionRepository
}

func (h *GetBalanceHandler) Handle(address common.Address) (*big.Int, error) {
	balance, err := h.repository.GetBalance(address)
	if err != nil {
		return nil, fmt.Errorf("error getting balance: %s", err)
	}

	return balance, nil
}

func NewGetBalanceHandler(repository storage.TransactionRepository) *GetBalanceHandler {
	return &GetBalanceHandler{repository: repository}
}
