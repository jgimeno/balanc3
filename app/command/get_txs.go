package command

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/domain"
	"github.com/jgimeno/balanc3/service/storage"
)

type GetTransactionsHandler struct {
	repository storage.TransactionRepository
}

func (h *GetTransactionsHandler) Handle(address common.Address) (*domain.TransactionRegister, error) {
	register, err := h.repository.GetTransactionRegister(address)
	if err != nil {
		return nil, fmt.Errorf("error getting transaction register: %s", err)
	}

	return register, nil
}

func NewGetTransactionsHandler(repository storage.TransactionRepository) *GetTransactionsHandler {
	return &GetTransactionsHandler{repository: repository}
}
