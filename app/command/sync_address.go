package command

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/jgimeno/balanc3/service/explorer"
	"github.com/jgimeno/balanc3/service/storage"
)

type SyncAddressHandler struct {
	chainExplorer explorer.ChainExplorer
	repository    storage.TransactionRepository
}

//Handle is not transactional, so it is not the best implementation, it should be created in a transactional way
//@TODO get it transactional
func (s *SyncAddressHandler) Handle(address common.Address) error {
	txsReg, err := s.chainExplorer.GetTransactions(address)
	if err != nil {
		return fmt.Errorf("error getting transactions when trying to sync: %s", err)
	}

	err = s.repository.SaveTransactionRegister(txsReg)
	if err != nil {
		return fmt.Errorf("error saving transaction to db when trying to sync: %s", err)
	}

	balance, err := s.chainExplorer.GetBalance(address)
	if err != nil {
		return fmt.Errorf("error getting balance when trying to sync: %s", err)
	}

	err = s.repository.SaveBalance(address, balance)
	if err != nil {
		return fmt.Errorf("error saving balance when trying to sync: %s", err)
	}

	return nil
}

func NewSyncAddressHandler(chainExplorer explorer.ChainExplorer, repository storage.TransactionRepository) *SyncAddressHandler {
	return &SyncAddressHandler{
		chainExplorer: chainExplorer,
		repository:    repository,
	}
}
